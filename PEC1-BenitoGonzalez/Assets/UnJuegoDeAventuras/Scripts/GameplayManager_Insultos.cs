﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class GameplayManager_Insultos : MonoBehaviour
{
    public Text HistoryText;
    public Transform AnswersParent;
    public GameObject ButtonAnswerPrefab;

    private Insultos currentNode;


    private void Start()
    {
        
        currentNode = Generador_Insultos.Datos_Turno();
        HistoryText.text = string.Empty;
        FillUi();
    }

    void FillUi()
    {
        HistoryText.text += "\n\n" + currentNode.InsultosPC;
        

        foreach (Transform child in AnswersParent.transform)
        {
            Destroy(child.gameObject);
        }

        var isLeft = true;
        var height = 50.0f;
        var index = 0;
        foreach (var answer in currentNode.InsultosPlayer)
        {
            var buttonAnswerCopy = Instantiate(ButtonAnswerPrefab, AnswersParent, true);
            

            var x = buttonAnswerCopy.GetComponent<RectTransform>().rect.x * 1.5f;
            buttonAnswerCopy.GetComponent<RectTransform>().localPosition = new Vector3(isLeft ? x : -x, height, 0);

            if (!isLeft)
                height += buttonAnswerCopy.GetComponent<RectTransform>().rect.y * 3.0f;
            isLeft = !isLeft;

            FillListener(buttonAnswerCopy.GetComponent<Button>(), index);

            buttonAnswerCopy.GetComponentInChildren<Text>().text = answer;

            
            
            index++;

            
        }

    
        
    }

    private void FillListener(Button button, int index)
    {
        button.onClick.AddListener(() => { AnswerSelected(index); });
        
    }

    private void AnswerSelected(int index)
    {
        HistoryText.text += "\n" + currentNode.InsultosPlayer[index];

        if (!currentNode.IsFinal)
        {
            currentNode = currentNode.NextNode[index];

            currentNode.OnNodeVisited?.Invoke();

            FillUi();
            
        }
        else
        {
            HistoryText.text += "\n" + "TOCA ESCAPE PARA CONTINUAR";
            
        }
    }

    


}
