﻿using System.Collections.Generic;
using UnityEngine;
using System;

public class Generador_Insultos 
{



    public static Insultos Datos_Turno()
    {

        int Wins = 0; // numero de victorias Player
        

        
        string[] insultosPC_1 = new string[] {"No eres muy inteligente!", "No sabes sumar 0101 + 1001, patético", "Mi IQ es 23433142...y el tuyo?", "Tu mente es limitada!"}; //lista de insultos que puede decir el PC en la ronda 1
        string[] insultosPC_2 = new string[] {"Eres débil!", "Vaya bracitos más pateticos!", "Te podría aniquilar, pero no quiero", "Tu fuerza es inexistente!"}; //lista de insultos que puede decir el PC en la ronda 2
        string[] insultosPC_3 = new string[] {"Tu familia es tan decepcionante como tu?", "Quieres que llame a tu mama?", "Tu padre y tu sois iguales! Calvos!", "Seguro que tu ADN no es el de un mono?"}; //lista de insultos que puede decir el PC en la ronda 3
        string[] insultosPC_4 = new string[] {"No creo en tu sistema de valores", "Tu moral es cuestionable y frágil!", "Hipócrita!", "Tus creencias son inutiles, humano!"}; //lista de insultos que puede decir el PC en la ronda 4
        string[] insultosPC_5 = new string[] {"Nadie te quiere...y yo menos!", "Tu novia existe? o te la has inventado?", "Eres feo con ganas!", "Te quedarás sólo en esta vida!"}; //lista de insultos que puede decir el PC en la ronda 5

        string[] insultosPlayer = new string[] {"Vas mucho al gym? Oh perdona,no tienes piernas", "Deja que te desenchufe y acabe con tu soledad", "Tu abuela era una calculadora, pringao!", "Mi cerebro es de verdad. Y el tuyo?", "Tú solo entiendes de 0s y 1s!"}; //lista de insultos que puede decir el Jugador

        
        var List_insultosPC = new []{insultosPC_1, insultosPC_2, insultosPC_3, insultosPC_4, insultosPC_5};



        List<Insultos> List_turnos = new List<Insultos>(); // Se utiliza para inicializar el programa con las primeras transparencias y opciones a seleccionar
        List<Insultos> List_turnos_Player = new List<Insultos>(); // Será donde se guarden los turnos con la estructura del jugador: 5 respuestas
        List<Insultos> List_turnos_PC = new List<Insultos>(); // Será donde se guarden los truenos con la estructura del PC: 1 respuesta


        var root = CreateTurno("Bienvenido! A insultarse!",new[]{"Let's go!"});
        var node_end_Win = CreateTurno("....El JUGADOOOR!!. Pulsa Esc!", new[]{"Salir del juego"});
        var node_end_Lost = CreateTurno("....LA MÁQUINAAA!!. Pulsa Esc!", new[]{"Salir del juego"});      
        var Final = CreateTurno("Bien jugado ambos! El ganador es....",new[]{"Continuar"});
        



        List_turnos.Add(root); // el primer elemento del List_turnos será el root;

        var random = new System.Random(); // se elige aleatoriamente quien empieza el duelo: Jugador o PC. Esta variable se utilizará para marcar el orden cada turno segun quien gane
        var flag = random.Next(2) == 1;


    
        

        if (flag == true) //empieza el PC. Así que se comunica que él empieza
        {

            var node_start = CreateTurno("...Empieza la máquina. Buena suerte!",new[]{"Continuar"});
            List_turnos.Add(node_start);
            
        }
        else // empieza el jugador. Asi que se comunica que el jugador empieza
        {
            
            var node_start = CreateTurno("...Empieza el JUGADOR. Buena suerte!",new[]{"Continuar"});
            List_turnos.Add(node_start);
        }






        
        root.NextNode[0] = List_turnos[1]; // root adquiere el valor del primer elemento de List_turno[], y así se irá pasando de una opción a otra.

        

        for (int round = 0; round <= 4; round++)  // Aquí se crea la secuencia de insultos tanto para PC como para jugador.
        {
            
            string[] insultosPC = List_insultosPC[round];
            
            int count = UnityEngine.Random.Range(0,4);
            var node_PC = CreateTurno(
            "Round " + round.ToString() + "\n" + "Turno PC: " + "\n" +  insultosPC[count], new[]{"Mi turno"}); // el insulto elegido por la máquina es aleatorio


            var node_Player = CreateTurno(
            "Round " + round.ToString() + "\n" + "Turno Jugador: " + "\n", insultosPlayer);




            //
            List_turnos_PC.Add(node_PC);
            List_turnos_Player.Add(node_Player);

            if(round == 4) // en la ultima ronda se le añade un turno final que es para concluir y mostrar porsteriormente si ha ganado el PC o la máquina
            {
                List_turnos_PC.Add(Final);
                List_turnos_Player.Add(Final);
            }


        
        }




        for (int round = 0; round <= 4; round++) // Aqui se genera el orden de los turnos en cada ronda segun se juegan.
        {


            if (flag == true && round == 0) // 1r ronda de todas.!!!!!!!!!!!!!!!!!!!!!! Es un caso especial porque se debe guardar el valor en list_turnos[1]. el PC ha sido elegido, por lo tanto empieza el turno
            {
                List_turnos[1].NextNode[0] = List_turnos_PC[round];
                List_turnos_PC[round].NextNode[0] = List_turnos_Player[round];

            }
            else if (flag == false && round == 0) // el Jugador ha sido elegido, por lo tanto empieza el turno
            {

                List_turnos[1].NextNode[0] = List_turnos_Player[round];
                for (int i=0; i < 5; i++)
                {
                    List_turnos_Player[round].NextNode[i] = List_turnos_PC[round];
                    if (i == 3)
                        List_turnos_Player[round].IsVictoria = true;


                }


            }
            if(List_turnos_Player[round].IsVictoria == true) // se incrementa el contador de victorias si el flag de victorias es true
                Wins = Wins + 1;



            


            if (flag == true && round != 0) // el PC ha ganado o ha sido elegido, por lo tanto empieza el turno
            {
                for (int i=0; i < 5; i++)
                {
                    if ( round==1 && i==0 // La respuesta correcta ronda 2 (valor 1) es la posicion 0 de insultosPlayer.
                        || round==2 && i==2 // La respuesta correcta ronda 3 (valor 2) es la posicion 2 de insultosPlayer.
                        || round==3 && i==4 // La respuesta correcta ronda 4 (valor 3) es la posicion 4 de insultosPlayer.
                        || round==4 && i==1) // La respuesta correcta ronda 5 (valor 4) es la posicion 1 de insultosPlayer.
                    {
                        //es victoria
                        List_turnos_Player[round].IsVictoria = true;
                        List_turnos_Player[round-1].NextNode[i] = List_turnos_PC[round]; // llega el turno anterior
                        List_turnos_PC[round].NextNode[0] = List_turnos_Player[round];
                    }
                    else
                    {
                         // no es victoria
                        List_turnos_Player[round-1].NextNode[i] = List_turnos_PC[round]; // llega el turno anterior
                        List_turnos_PC[round].NextNode[0] = List_turnos_Player[round];
                    }

                    

      




                }       

                if(List_turnos_Player[round].IsVictoria == true) // se incrementa el contador de victorias si el flag de victorias es true
                Wins = Wins + 1; 

             
            }

            else if (flag == false && round != 0) // el Jugador ha ganado o ha sido elegido, por lo tanto empieza el turno
            {
                for (int i=0; i < 5; i++)
                {
                    if ( round==1 && i==0 // La respuesta correcta ronda 2 (valor 1) es la posicion 0 de insultosPlayer.
                        || round==2 && i==2 // La respuesta correcta ronda 3 (valor 2) es la posicion 2 de insultosPlayer.
                        || round==3 && i==4 // La respuesta correcta ronda 4 (valor 3) es la posicion 4 de insultosPlayer.
                        || round==4 && i==1) // La respuesta correcta ronda 5 (valor 4) es la posicion 1 de insultosPlayer.
                    {
                        //es victoria
                        List_turnos_Player[round].IsVictoria = true;
                        List_turnos_PC[round-1].NextNode[0] = List_turnos_Player[round]; // llega el turno anterior
                        List_turnos_Player[round].NextNode[i] = List_turnos_PC[round];
                    }
                    else
                    {
                         // no es victoria
                        List_turnos_PC[round-1].NextNode[0] = List_turnos_Player[round]; // llega el turno anterior
                        List_turnos_Player[round].NextNode[i] = List_turnos_PC[round];
                    }

                }

                    if(List_turnos_Player[round].IsVictoria == true) // se incrementa el contador de victorias si el flag de victorias es true
                        Wins = Wins + 1; 


            }






                        
            if (flag == false && round == 4) // ULTIMA RONDA.!!!!!!!!!!! Aquí se decide quien gana. Turno anterior acabo el PC
            {

                List_turnos_PC[round].NextNode[0] = List_turnos_PC[round+1];
                if (Wins >=3 )
                    List_turnos_PC[round+1].NextNode[0] = node_end_Win;
                else if (Wins < 3)
                    List_turnos_PC[round+1].NextNode[0] = node_end_Lost;
 


            }
            else if (flag == true && round == 4) // ULTIMA RONDA.!!!!!!!!!!! Aquí se decide quien gana. Turno anterior acabo el jugador
            {

                List_turnos_Player[round].NextNode[0] = List_turnos_Player[round+1];
                List_turnos_Player[round].NextNode[1] = List_turnos_Player[round+1];
                List_turnos_Player[round].NextNode[2] = List_turnos_Player[round+1];
                List_turnos_Player[round].NextNode[3] = List_turnos_Player[round+1];
                List_turnos_Player[round].NextNode[4] = List_turnos_Player[round+1];
                if (Wins >=3 )
                    List_turnos_Player[round+1].NextNode[0] = node_end_Win;
                else if (Wins < 3)
                    List_turnos_Player[round+1].NextNode[0] = node_end_Lost;


            }
                        

                

        }

             



        node_end_Lost.IsFinal = true;
        node_end_Win.IsFinal = true;
        return root;


    



    }

        


        


        




    


    public static Insultos CreateTurno(string insultosPC, string[] insultosPlayer)
    {
        var node = new Insultos
        {
            InsultosPC = insultosPC,
            InsultosPlayer = insultosPlayer,
            NextNode = new Insultos[insultosPlayer.Length],
            IsVictoria = false,


            
        };
        return node;


    }




}
