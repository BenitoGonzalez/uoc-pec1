UOC-PEC1


El Juego parte de la pec- template1 con 3 escenas:

- New Game: Es el meni princiapl donde se puede selecciona "Nueva Partida" para empezar
- Game: Es la escena principal, original del pec-template1
- GameOver: Es la escena que saldrá al salir del juego (No esta implementado!)


El juego consiste en pelearse con la máquina.

Al empezar el duelo, se selecciona aleatoriamente quien empieza: Jugador o Máquina/PC.
Entonces se juegan 5 rondas, donde jugador y máquina se insultan.
El jugador para cada ronda tien solo una opción correcta.

Una vez se empieza la idea es que quien gane 3 rondas, gane el juego.


Para ello hay 3 scripts principales:

- Insultos.cs: esta sería la clase con la estructura de cada turno. Heredado de StoryNode
	- insultosPC (lo que la máquina diria en cada turno), 
	- insultosPlayer (los insultos que el jugador puede decir)
	- isVictoria (flag para indicar que ese turno lo gana el jugador)
	- isFinal (para inidcar si es ya el turno final)
	- NextNode (para indicar cual será el siguient turno)

- Generador_Insultos.cs: en este script se generan los turnos con sus datos a partir de la función Datos_Turno()
En esta función primeramente se inicializan los strings con los datos que se van a utilizar ya sea de la máquina o del jugador
Despues se generan los turnos mediante la función CreatTurno() y despues ya se elige el orden de como se va a ejecutar el programa segun si empieza el jugador o la máquina.



	
- GameplayManager_Insultos.cs: A partir del template GameplayManager de pec1-template, se ajusta para poder utilizar la interfaz e ir generando cada turno con sus botones correspondientes.
